package mobile.agima.ru.iboxapp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import mobile.agima.ru.iboxapp.R;

public class MenuNavigationDrawerArrayAdapter extends ArrayAdapter<String> {

    Context context;
    String[] results;
    LayoutInflater inflater;

    public MenuNavigationDrawerArrayAdapter(Context context, String[] results){
        super(context, R.layout.drawer_list_item,results);
        this.context = context;
        this.results = results;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int checkedPosition = ((ListView)parent).getCheckedItemPosition();
        if (checkedPosition == -1) checkedPosition = 0;

        View rootView = inflater.inflate(R.layout.drawer_list_item,parent,false);
        ImageView iconImageView = (ImageView)rootView.findViewById(R.id.iconImageView);
        TextView titleTextView = (TextView)rootView.findViewById(R.id.titleTextView);
        titleTextView.setText(results[position]);
        switch (position){
            case 0: iconImageView.setBackgroundResource(R.drawable.ic_home);break;
            case 1: iconImageView.setBackgroundResource(R.drawable.ic_insurance);break;
            case 2: iconImageView.setBackgroundResource(R.drawable.ic_request);break;
            case 3: iconImageView.setBackgroundResource(R.drawable.ic_map);break;
            case 4: iconImageView.setBackgroundResource(R.drawable.ic_news);break;
        }
        if (checkedPosition == position){
            rootView.setBackgroundResource(R.drawable.main_menu_item_selected_with_top_divider);
        }
        return rootView;
    }
}
